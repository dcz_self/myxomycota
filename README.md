Myxomycota: slime paint
=========

Today, it's just paint in Bevy.

Tomorrow, it will be a slime mould simulator.

Running
----------

```
cd Myxomycota
cargo run
```

License
---------

AGPL version 3.0 or later.
