use image;
use imageproc::drawing;


pub struct Position {
    pub x: u16,
    pub y: u16,
}

impl From<Position> for (i32, i32) {
    fn from(p: Position) -> Self {
        (p.x as i32, p.y as i32)
    }
}

type BinaryMap = image::GrayImage;

#[derive(Clone)]
pub struct State {
    pub size: (u16, u16),
    pub mould: BinaryMap,
}

impl State {
    pub fn new(size: (u16, u16)) -> State {
        State {
            size: size.clone(),
            mould: BinaryMap::from_pixel(size.0 as u32, size.1 as u32, image::Luma::from([0])),
        }
    }
    pub fn splat(self, position: (i32, i32)) -> Self {
        State {
            mould: drawing::draw_filled_circle(&self.mould, position.into(), 3, image::Luma::from([1])),
            ..self
        }
    }
}
