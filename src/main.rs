use bevy::{
    prelude::*,
    reflect::TypeUuid,
    render::{
        pipeline::{PipelineDescriptor, RenderPipeline},
        render_graph::{base, AssetRenderResourcesNode, RenderGraph},
        renderer::RenderResources,
        shader::{ShaderStage, ShaderStages},
        texture::{ TextureDimension, TextureFormat, Extent3d },
    },
    utils::Duration,
};
use std::num::Wrapping;

mod game;
 

/// Paint with slime.
fn main() {
    App::build()
        .add_plugins(DefaultPlugins)
        .add_asset::<Picture>()
        .insert_resource(game::State::new((256, 256)))
        .add_startup_system(setup.system())
        .add_startup_system(setup_timer.system())
        .add_startup_system(setup_cursor.system())
        .add_system(timer_tick.system())
        .add_system(mouse_move.system())
        .add_system(mouse_click.system())
        .add_system(update_picture.system())
        .run();
}

pub struct MainCamera;

/// The component that represents the mutable texture.
/// It is a resource at the same time.
/// Because it's an asset, it needs an unique UUID.
#[derive(RenderResources, Default, TypeUuid)]
#[uuid = "926b9368-a7ee-42de-9725-152a8b65b37b"]
struct Picture {
    pub texture: Handle<Texture>,
}

const VERTEX_SHADER: &str = r#"
#version 450

layout(location = 0) in vec3 Vertex_Position;
layout(location = 1) in vec3 Vertex_Normal;
layout(location = 2) in vec2 Vertex_Uv;

layout(location = 0) out vec2 uv_Position;

layout(set = 0, binding = 0) uniform CameraViewProj {
    mat4 ViewProj;
};
layout(set = 1, binding = 0) uniform Transform {
    mat4 Model;
};

void main() {
    gl_Position = ViewProj * Model * vec4(Vertex_Position, 1.0);
    uv_Position = Vertex_Uv;
}
"#;

const FRAGMENT_SHADER: &str = r#"
#version 450

layout(location = 0) in vec2 uv_Position;
layout(location = 0) out vec4 o_Target;

layout(set = 2, binding = 0) uniform texture2D Picture_texture;
layout(set = 2, binding = 1) uniform sampler Picture_texture_sampler;

void main() {
    o_Target = texture(sampler2D(Picture_texture, Picture_texture_sampler), uv_Position);
}
"#;

fn setup(
    mut commands: Commands,
    mut pipelines: ResMut<Assets<PipelineDescriptor>>,
    mut shaders: ResMut<Assets<Shader>>,
    mut textures: ResMut<Assets<Texture>>,
    mut render_graph: ResMut<RenderGraph>,
    mut my_textures: ResMut<Assets<Picture>>,
) {
    // Create a texture with varying shades of red.
    let extent = Extent3d {
        width: 256,
        height: 256,
        depth: 1,
    };
    let data = (0..(extent.volume()))
        .flat_map(|_| vec![255, 255, 255, 255])
        .collect::<Vec<u8>>();

    let texture = Texture::new_fill(
        extent.clone(),
        TextureDimension::D2,
        &data,
        TextureFormat::Rgba8UnormSrgb,
    );
    
    let texture_handle = textures.add(texture);

    // Create a new shader pipeline.
    let pipeline_handle = pipelines.add(PipelineDescriptor::default_config(ShaderStages {
        vertex: shaders.add(Shader::from_glsl(ShaderStage::Vertex, VERTEX_SHADER)),
        fragment: Some(shaders.add(Shader::from_glsl(ShaderStage::Fragment, FRAGMENT_SHADER))),
    }));

    // Add an AssetRenderResourcesNode to our Render Graph.
    // This will bind Picture resources to shaders.
    render_graph.add_system_node(
        "my_texture_node",
        AssetRenderResourcesNode::<Picture>::new(true),
    );
    // Add a Render Graph edge connecting our new node called "my_texture_node" to the main pass node.
    // This ensures "my_texture_node" runs before the main pass.
    render_graph
        .add_node_edge("my_texture_node", base::node::MAIN_PASS)
        .unwrap();

    commands
        .spawn_bundle(OrthographicCameraBundle::new_2d())
        .insert(MainCamera);
    
    let my_texture_handle = my_textures.add(Picture { texture: texture_handle.clone() } );
    // The render pipeline, and so, "my_texture_node" will pick up Picture.
    // For that, a handle to Picture must be another coponent on the same entity.
    // Here, we create an entity that has, among others, two components that work together:
    // - a pipeline with "my_texture_node",
    // - a handle to Picture.
    commands
        .spawn_bundle(SpriteBundle {
            render_pipelines: RenderPipelines::from_pipelines(vec![RenderPipeline::new(
                pipeline_handle.clone(),
            )]),
            transform: Transform::from_scale(Vec3::splat(512.0)),
            ..Default::default()
        })
        .insert(my_texture_handle.clone());
    commands.insert_resource(Picture { texture: texture_handle.clone() } );
    commands.insert_resource(my_texture_handle);
}

struct TickTrack {
    count: Wrapping<u8>,
    timer: Timer,
}

fn setup_timer(
    mut commands: Commands,
) {
    commands.insert_resource(TickTrack {
        count: Wrapping(0),
        timer: Timer::new(Duration::from_secs(1), true)
    });
}

fn timer_tick(
    time: Res<Time>,
    mut timer: ResMut<TickTrack>,
    mut textures: ResMut<Assets<Texture>>,
    mut my_textures: ResMut<Assets<Picture>>,
    pic: Res<Handle<Picture>>,
) {
    timer.timer.tick(time.delta());
    if timer.timer.finished() {
        timer.count += Wrapping(1);

        // `get_mut` triggers a modify event on the Picture asset,
        // which is how the AssetRenderResourcesNode knows about the change.
        // AssetRenderResourcesNode<Picture> doesn't know what is inside Picture,
        // so it will not pick up changes to the inner Texture on its own.
        let my_texture = my_textures.get_mut(&*pic).unwrap();
        let texture = textures.get_mut(&my_texture.texture).unwrap();
        let idx = (timer.count.0 as usize) * 4 % texture.size.volume();
        // Each timer interval, one pixel turns black.
        texture.data[idx..(idx + 3)]
            .iter_mut()
            .for_each(|i| *i = 0);
    }
}

fn draw_state(state: &game::State, texture: &mut Texture) {
    /*let mut img = image::flat::FlatSamples {
        samples: &mut texture.data,
        layout: image::flat::SampleLayout {
            channels: 4,
            channel_stride: 4,
            width: state.size.0 as u32,
            width_stride: 1,
            height: state.size.1 as u32,
            height_stride: state.size.1 as usize,
        },
        color_hint: None,
    };
    let img = img.as_view_mut::<image::Rgba<u8>>();*/
    let new_img = imageproc::map::map_colors(
        &state.mould,
        |p| {
            if p.0[0] == 1 { image::Rgba([0, 0, 0, 255]) }
            else { image::Rgba([255, 255, 255, 255]) }
        }
    );
    texture.data = new_img.into_raw();
}

fn update_picture(
    state: Res<game::State>,
    mut textures: ResMut<Assets<Texture>>,
    mut my_textures: ResMut<Assets<Picture>>,
    pic: Res<Handle<Picture>>,
) {
    let my_texture = my_textures.get_mut(&*pic).unwrap();
    let texture = textures.get_mut(&my_texture.texture).unwrap();
    draw_state(&*state, texture)
}

pub struct FollowsMouse;
pub struct Brush;
type Canvas = Handle<Picture>;

fn setup_cursor(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands
        .spawn_bundle(SpriteBundle {
            material: materials.add(Color::rgba(0., 0., 0., 0.5).into()),
            sprite: Sprite {
                size: Vec2::new(20.0, 20.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(FollowsMouse)
        .insert(Brush);
}

pub fn mouse_move(
    mut cursor_moved: EventReader<CursorMoved>,
    mut positions: Query<&mut Transform, With<FollowsMouse>>,
    wnds: Res<Windows>,
    q_camera: Query<&Transform, (With<MainCamera>, Without<FollowsMouse>)>
) {
    let camera_transform = q_camera.iter().next().unwrap();
    
    for event in cursor_moved.iter() {
        for mut position in positions.iter_mut() {
            let wnd = wnds.get(event.id).unwrap();
            let size = Vec2::new(wnd.width() as f32, wnd.height() as f32);

            // the default orthographic projection is in pixels from the center;
            // just undo the translation
            let p = event.position - size / 2.0;

            let pos_wld = camera_transform.compute_matrix() * p.extend(0.0).extend(1.0);
            position.translation = Vec3::new(pos_wld.x, pos_wld.y, position.translation.z);
        }
    }
}

fn mouse_click(
    mouse_button_input: Res<Input<MouseButton>>,
    brush_positions: Query<&Transform, (With<FollowsMouse>, With<Brush>)>,
    canvas_positions: Query<&Transform, With<Canvas>>,
    mut state: ResMut<game::State>,
) {
    let scale = 0.5; // FIXME: derive from canvas size vs sprite size.
    if mouse_button_input.pressed(MouseButton::Left) {
        for brush_position in brush_positions.iter() {
            for canvas_position in canvas_positions.iter() {
                let position_from_center = brush_position.translation - canvas_position.translation;
                let position_in_canvas = position_from_center * Vec3::new(scale, -scale, 0.0)
                    + Vec3::new(state.size.0 as f32, state.size.1 as f32, 0.0) / 2.0;
                *state = state.clone().splat((position_in_canvas.x as i32, position_in_canvas.y as i32));
                println!("splot {:?}", position_in_canvas);
            }
        }
    }
}
